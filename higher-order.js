// This function should execute the callback function the number of times specified.
// When the function is being executed, the repetition number (i.e. 1 for the first call)
// should be passed to the callback.

const repeatFn = (times, callback) => {
  for (let index = 0; index < times; index++) {
    callback(index);
  }

}

const addThree = (number) => {
  const result = (number += 3);
  console.log(result);

  return result;
}

[1, 2, 3, 4, 5, 6].forEach((e, i) => {

  addThree(e);

});


repeatFn(10, addThree);

// Test repeatFn
const addButton = num => {
  const button = document.createElement('button');
  button.innerText = `Button ${num}`;
  document.querySelector('body').appendChild(button);
};
repeatFn(6, addButton);

const toThePower = (num, pow) => {
  let product = 1;
  repeatFn(pow, () => {
    product += product * num;
  });
  return product;
};

console.log(toThePower(3, 3));
